    Option Explicit     
    Private Const adReadAll = -1  
    Private Const adSaveCreateOverWrite = 2  
    Private Const adTypeBinary = 1  
    Private Const adTypeText = 2  
    Private Const adWriteChar = 0  
    Dim cs 
	 
    Private Sub UTF8toANSI(ByVal UTF8FName, ByVal ANSIFName, ByVal cs)  
      Dim strText  
      
      With CreateObject("ADODB.Stream")  
      .Open  
      .Type = adTypeBinary  
      .LoadFromFile UTF8FName  
      .Type = adTypeText  
      .Charset = "utf-8"  
      strText = .ReadText(adReadAll)  
      .Position = 0  
      .SetEOS  
      .Charset = cs
      .WriteText strText, adWriteChar  
      .SaveToFile ANSIFName, adSaveCreateOverWrite  
      .Close  
      End With  
    End Sub  
    if WScript.Arguments.Count > 2 then cs = WScript.Arguments(2) else cs = "_autodetect"
	
    UTF8toANSI WScript.Arguments(0), WScript.Arguments(1), cs
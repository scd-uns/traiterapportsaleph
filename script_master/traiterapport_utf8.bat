@echo off
setlocal enabledelayedexpansion
set script_master=script_master\
echo === Script de création de rapport en HTML ===
echo Opération préalable :
echo Dans Aleph, sélectionner le rapport à traiter et faites en une copie locale (cliquez sur l'icône "Flèche vers le haut")
echo Par défaut un fichier HTML sera créé à partir du rapport issu d'Aleph et enregistré sur votre Bureau
echo Le fichier config.ini permet de modifier l'emplacement de destination
CHOICE /C ON /M "Voulez-vous continuer la procédure?"
If Errorlevel 2 Goto :eof

REM https://ss64.com/nt/delayedexpansion.html
echo === Recupération des paramètres ===
call %~dp0..\%script_master%get-param.bat repertoires AlephDir AlephDir
call %~dp0..\%script_master%get-param.bat repertoires sourcerelativefilepath1 sourcerelativefilepath1
call %~dp0..\%script_master%get-param.bat repertoires sourcerelativefilepath2 sourcerelativefilepath2
call %~dp0..\%script_master%get-param.bat repertoires sourcerelativefilepath3 sourcerelativefilepath3
call %~dp0..\%script_master%get-param.bat repertoires sourcerelativefilepath4 sourcerelativefilepath4
call %~dp0..\%script_master%get-param.bat repertoires sourcerelativefilepath5 sourcerelativefilepath5
call %~dp0..\%script_master%get-param.bat repertoires sourcerelativefilepath6 sourcerelativefilepath6
call %~dp0..\%script_master%get-param.bat repertoires outputDir outputDir
call %~dp0..\%script_master%get-param.bat repertoires tempDir tempDir
call %~dp0..\%script_master%get-param.bat repertoires xslColodus xslColodus
call %~dp0..\%script_master%get-param.bat repertoires xslPilon xslPilon
call %~dp0..\%script_master%get-param.bat repertoires xslRecotation xslRecotation
call %~dp0..\%script_master%get-param.bat repertoires xslSudoc xslSudoc
call %~dp0..\%script_master%get-param.bat java JavaMax JavaMax
call %~dp0..\%script_master%get-param.bat parsing firstline firstline

echo === Vérification des paramètres obligatoires ===
set listparams=AlephDir sourcerelativefilepath1 sourcerelativefilepath2 sourcerelativefilepath3 outputDir tempDir xslColodus xslPilon xslRecotation xslSudoc javaMax firstline
(for %%a in (%listparams%) do ( 
   if not [!%%a!] == [] (echo paramètre %%a défini : !%%a!) else (call :error "Paramètre non défini : %%a")
))

echo === ajout de \ aux répertoires si besoin ===
call :corrigerepertoires "!AlephDir!" AlephDir
call :corrigerepertoires "!sourcerelativefilepath1!" sourcerelativefilepath1
call :corrigerepertoires "!sourcerelativefilepath2!" sourcerelativefilepath2
call :corrigerepertoires "!sourcerelativefilepath3!" sourcerelativefilepath3
call :corrigerepertoires "!outputDir!" outputDir
call :corrigerepertoires "!tempDir!" tempDir

echo === Vérification de l'existence des répertoires ===
if exist "!AlephDir!" (echo Le répertoire !AlephDir! existe) else (call :error "Répertoire inconnu : !AlephDir!")
if exist "!outputDir!" (echo Le répertoire !outputDir! existe) else (call :error "Répertoire inconnu : !outputDir!")
if exist "!tempdir!" (echo Le répertoire !tempDir! existe) else (call :error "Répertoire inconnu : !tempDir!")

echo === Choix du type de rapport à traiter ===
call :get-type-rapport typeRapport
	
echo === Nom du fichier ===
if [%1]==[] (call :get-filename "!typeRapport!" filename) else (set filename=%1)
echo fichier=!filename!
if  [!filename!] == [] (call :error "Pas de nom de fichier !")
set tempfilename=temp%filename%.xml
set outputfilename=%filename%.html

echo === Choix de la feuille de style ===
REM call :get-feuille-de-style "!typeRapport!" "!xslColodus!" "!xslPilon!" "!xslRecotation!" feuilleDeStyle
call :get-feuille-de-style "!typeRapport!" "!xslColodus!" "!xslPilon!" "!xslSudoc!" feuilleDeStyle

echo === Vérification de l'existence du fichier source ===
call :get-file "!AlephDir!" "!sourcerelativefilepath1!" "!sourcerelativefilepath2!" "!sourcerelativefilepath3!" "!sourcerelativefilepath4!" "!sourcerelativefilepath5!" "!sourcerelativefilepath6!" "!filename!" filepath
if  [!filepath!] == [] (call :error "Fichier source introuvable : !filename!") else (echo Fichier source !filename! trouvé dans !filepath!)

echo === Vérification de l'existence du fichier destination ===
if exist "!outputDir!!outputfilename!" (
	echo Le fichier destination !outputfilename! existe déjà dans le répertoire !outputDir!.
	CHOICE /C ON /M "Voulez-vous l'écraser ?"
	If Errorlevel 2 Goto :eof
	)

echo === Nettoyage de la première ligne et création du fichier temporaire ===
REM supprime la 1re ligne du rapport : ## - XML_XSL
REM syntaxe de findstr https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-xp/bb490907(v=technet.10)
REM /v inverse le motif /r active les expression régulières et /c: capture un motif avec des espaces
REM Hack: on saisit la chaîne à la main au lieu d'utiliser la variable %firstline% sinon ça plante. A creuser!
findstr /v /r /c:"^## - XML_XSL$" "!filepath!" > "!tempDir!!tempfilename!"
if exist "!tempDir!!tempfilename!" (echo Fichier temporaire !tempfilename! créé dans le répertoire "!tempDir!") else (echo Erreur lors de la création du fichier temporaire)

echo === Création du fichier HTML définitif===
echo "java -Xmx!javaMax! -cp !AlephDir!Alephcom\bin\saxon8.jar net.sf.saxon.Transform -t -o !outputDir!!outputfilename! !tempDir!!tempfilename! !AlephDir!!feuilleDeStyle!"
java -Xmx!javaMax! -cp "!AlephDir!Alephcom\bin\saxon8.jar" net.sf.saxon.Transform -t -o "!outputDir!!outputfilename!" "!tempDir!!tempfilename!" "!AlephDir!!feuilleDeStyle!"
if exist "!outputDir!!outputfilename!" (echo Fichier définitif !outputfilename! créé dans le répertoire !outputDir!) else (echo Erreur lors de la création du fichier définitif)

echo === Suppression du fichier temporaire ===
REM del "!tempDir!!tempfilename!"
set /p end=Appuyez sur [Entrée] pour quitter le script
goto :eof

REM =====================================================
REM FONCTIONS
REM =====================================================

:error <message>
	echo.
	echo ==== ERREUR : %~1 ====
	echo ==== ARRET PREMATURE DU SCRIPT ====
	pause
	exit 1
	
:get-type-rapport <result>
REM Trois types de rapports supportés : Colodus (hebdomadaire), pilon (mensuel), chargement Sudoc, et rapport personnalisé
	set coderapport=
	echo Choix du type de rapport à traiter :
	echo     1 : Colodus (hebdomadaire)
	echo     2 : Pilon (mensuel)
	echo     3 : Chargement Sudoc
REM le rapport 3 remplace "recotation hebdomadaire BU LASH"
	echo     4 : Personnalisé
	CHOICE /C 1234 /M "Saisissez le chiffre correspondant au type de rapport"
	If Errorlevel 1 (set coderapport=colodus)
	If Errorlevel 2 (set coderapport=pilon)
	If Errorlevel 3 (set coderapport=sudoc)
REM anciennement If Errorlevel 3 (set coderapport=recotation)
	If Errorlevel 4 (set coderapport=personnalise)	
	set %~1=!coderapport!
	goto :eof

:get-filename <typeRapport> <result>
REM La structure des noms de fichier dépend du type de rapport à traiter
	set p=%~1
	set t=
	if [!p!]==[colodus] (call :get-filename-colodus t)
	if [!p!]==[pilon] (call :get-filename-pilon t)
	if [!p!]==[sudoc] (call :get-filename-sudoc t)
REM anciennement if [!p!]==[recotation] (call :get-filename-recotation t)
	if [!p!]==[personnalise] (call :get-filename-personnalise t)
	set %~2=!t!
	goto :eof

:get-feuille-de-style <typeRapport> <xslColodus> <xslPilon> <xslSudoc> <result>
REM :get-feuille-de-style <typeRapport> <xslColodus> <xslPilon> <xslRecotation> <result>
	set p=%~1
	set xc=%~2
	set xp=%~3
	set xs=%~4
REM set xr=%~4
	set t=
	if [!p!]==[colodus] (set t=!xc!)
	if [!p!]==[pilon] (set t=!xp!)
REM if [!p!]==[recotation] (set t=!xr!)
	if [!p!]==[sudoc] (set t=!xs!)
	if [!p!]==[personnalise] (
		echo Choix de la feuille de style XSL à utiliser:
		echo     1 : Colodus ^(feuille 51^)
		echo     2 : Pilon ^(feuille 60^)
		echo     3 : Chargement Sudoc ^(feuille sudoc-report^)
REM echo     3 : Recotation ^(feuille 52^)
		CHOICE /C 123 /M "Saisissez le chiffre correspondant à la feuille de style"
		If Errorlevel 1 (set t=!xc!)
		If Errorlevel 2 (set t=!xp!)
		If Errorlevel 3 (set t=!xs!)
REM If Errorlevel 3 (set t=!xr!)
		)
	set %~5=!t!
	goto :eof

:get-filename-colodus <result>
REM Création du nom de fichier à chercher (pour rapports Colodus)
rem structure : 
rem préfixe 2colodus date (AAAAMMJJ)
rem ex: medp2colodus20181010
rem préfixes :
rem medp   Médecine Pasteur
rem sja    SJA
rem lash   LASH
rem staps  STAPS
rem scien  Sciences
rem droit  Droit
rem lc     LearningCentreSophiaTech
	set codebu=
	echo Choix de la BU concernée :
	echo     1 : Droit
	echo     2 : LASH
	echo     3 : Saint-Jean d'Angély
	echo     4 : Sciences
	echo     5 : STAPS
	echo     6 : Médecine Pasteur
	echo     7 : LCSophiaTech
	CHOICE /C 1234567 /M "Saisissez le chiffre correspondant à la BU"
	If Errorlevel 1 (set codebu=droit)
	If Errorlevel 2 (set codebu=lash)
	If Errorlevel 3 (set codebu=sja)
	If Errorlevel 4 (set codebu=scien)
	If Errorlevel 5 (set codebu=staps)
	If Errorlevel 6 (set codebu=medp)
	If Errorlevel 7 (set codebu=lc)
	echo Choix de la date :
	:labeldate
	set /p date=Saisissez la date du rapport (AAAAMMJJ) et appuyez sur [Entrée] : 
	echo !date! | findstr /v /r "^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9].$" >nul 2>&1 && (
		echo Date invalide. Merci de recommencer !
		goto :labeldate
	)
	set %~1=!codebu!2colodus!date!
	goto :eof
	
:get-filename-pilon <result>
REM Création du nom de fichier à chercher (pour rapports pilon)
rem structure : 
rem pilon.nom_bu.date (AAAAMMJJ)
rem ex: pilon.lc.20180901
rem nom des BUs :
rem medecine   Médecine Pasteur
rem sja        SJA
rem lash       LASH
rem staps      STAPS
rem sciences   Sciences
rem droit      Droit
rem lc         LearningCentreSophiaTech
	set codebu=
	echo Choix de la BU concernée :
	echo     1 : Droit
	echo     2 : LASH
	echo     3 : Saint-Jean d'Angély
	echo     4 : Sciences
	echo     5 : Médecine Pasteur
	echo     6 : LCSophiaTech
	CHOICE /C 123456 /M "Saisissez le chiffre correspondant à la BU"
	If Errorlevel 1 (set codebu=droit)
	If Errorlevel 2 (set codebu=lash)
	If Errorlevel 3 (set codebu=sja)
	If Errorlevel 4 (set codebu=sciences)
	If Errorlevel 5 (set codebu=medecine)
	If Errorlevel 6 (set codebu=lc)
	echo Choix de la date :
	:labeldate
	set /p date=Saisissez la date du rapport (AAAAMMJJ) et appuyez sur [Entrée] : 
	echo !date! | findstr /v /r "^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9].$" >nul 2>&1 && (
		echo Date invalide. Merci de recommencer !
		goto :labeldate
	)
	set %~1=pilon.!codebu!.!date!
	goto :eof

:get-filename-recotation <result>
REM Création du nom de fichier à chercher (pour rapports recotation philo LASH)
rem structure : 
rem lash.recotation.philo.date (AAAAMMJJ)
rem ex: lash.recotation.philo.20190212
	set codebu=lash
	set fonds=philo
	echo Attention: le nom du fichier déposé sur le PC doit répondre à la structure lash.recotation.philo.[date]
	echo Choix de la date :
	:labeldate
	set /p date=Saisissez la date du rapport (AAAAMMJJ) et appuyez sur [Entrée] : 
	echo !date! | findstr /v /r "^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9].$" >nul 2>&1 && (
		echo Date invalide. Merci de recommencer !
		goto :labeldate
	)
	set %~1=!codebu!.recotation.!fonds!.!date!
	goto :eof

:get-filename-sudoc <result>
REM Création du nom de fichier à chercher
rem structure : 
rem rapport_TR42R[NUMERO]A001.RAW
rem [NUMERO] est une séquence de 4 chiffres. Ex : 4926
	set init=rapport_TR42R
	set fin=A001
	set ext=RAW
	echo Attention: le nom du fichier déposé sur le PC doit répondre à la structure rapport_TR42[NUMERO SUR 4 CHIFFRES]A001.RAW
	echo Choix du numéro sur 4 chiffres :
	:labenum
	set /p num=Saisissez le numéro sur 4 chiffres (ex: 4926) et appuyez sur [Entrée] : 
	echo !num! | findstr /v /r "^[0-9][0-9][0-9][0-9].$" >nul 2>&1 && (
		echo Numéro invalide. Merci de recommencer !
		goto :labenum
	)
	set %~1=!init!!num!!fin!.!ext!
	goto :eof


:get-filename-personnalise <result>
REM Création du nom de fichier à chercher (personnalisé)
	set /p nom=Saisissez le nom du fichier déposé sur le PC et appuyez sur [Entrée] :
	set %~1=!nom!
	goto :eof

:get-file <aleph> <path1> <path2> <path3> <path4> <path5> <path6> <file> <result>
REM cherche le fichier source dans différents sous-répertoires du répertoire Aleph
rem FIXME ne fonctionne pas avec setlocal/endlocal
rem	setlocal
	set t=
	set p=%~1%~2%~8
	if exist "!p!" (if [!t!]==[] (set t=!p!))
	set p=%~1%~3%~8
	if exist "!p!" (if [!t!]==[] (set t=!p!))
	set p=%~1%~4%~8
	if exist "!p!" (if [!t!]==[] (set t=!p!))
	set p=%~1%~5%~8
	if exist "!p!" (if [!t!]==[] (set t=!p!))
	set p=%~1%~6%~8
	if exist "!p!" (if [!t!]==[] (set t=!p!))
	set p=%~1%~7%~8
	if exist "!p!" (if [!t!]==[] (set t=!p!))
	set %~9=!t!
rem endlocal
	goto :eof

:corrigerepertoires <repertoire> <resultat>
REM ajoute un \ après le nom d'un répertoire issu de config.ini s'il n'en comporte pas déjà
	set t=%~1
	if NOT [%t:~-1%]==[\] (
	set %~2=%~1\
	) else (
	set %~2=%~1
	)
	goto :eof

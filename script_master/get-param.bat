REM get-param <section> <key> <result>
REM lecture du fichier config.ini

    set %~3=
	setlocal
	set configfile=%~dp0..\config.ini
	set insection=
	for /f "usebackq eol=; tokens=*" %%a in ("%configfile%") do (
		set line=%%a
		if !insection!==1 (
			for /f "tokens=1,* delims==" %%b in ("!line!") do (			
				if /i "%%b"=="%2" (				
					endlocal
					set %~3=%%c
					goto :eof
				)
			)
		)
		if "!line:~0,1!"=="[" (
			for /f "delims=[]" %%b in ("!line!") do (
				if /i "%%b"=="%1" (
					set insection=1
				) else (
					set insection=
				)
			)
		)
	)
endlocal

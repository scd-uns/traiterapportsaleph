@echo off
setlocal enabledelayedexpansion
set utils=utils\
set script_master=script_master\
set script_poste=script_poste\
set encodage=
call %~dp0%script_master%get-param.bat encodagewindows encodage encodage
if not exist %~dp0%script_poste% (
	mkdir %~dp0%script_poste%
)
 
if [!encodage!]==[utf-8] (
	%~dp0%script_master%traiterapport_utf8.bat
	goto :eof
)
if [!encodage!]==[ibm850] (
    if not exist "%~dp0traiterapport_ibm850.bat" (
		echo "creation du script en encodage IBM850 a partir du script en UTF8"
		%~dp0%utils%utf8toansi.vbs %~dp0%script_master%traiterapport_utf8.bat %~dp0%script_poste%traiterapport_ibm850.bat ibm850
		)
	%~dp0%script_poste%traiterapport_ibm850.bat
	goto :eof
)
if [!encodage!]==[windows-1252] (
    if not exist "%~dp0traiterapport_win1252.bat" (
		echo "creation du script en encodage Windows1252 a partir du script en UTF8"
		%~dp0%utils%utf8toansi.vbs %~dp0%script_master%traiterapport_utf8.bat %~dp0%script_poste%traiterapport_win1252.bat windows-1252
		)
	%~dp0%script_poste%traiterapport_win1252.bat
	goto :eof
)
if [!encodage!]==[iso-8859-1] (
    if not exist "%~dp0traiterapport_iso88591.bat" (
		echo echo "creation du script en encodage iso-8859-1 a partir du script en UTF8"
		%~dp0%utils%utf8toansi.vbs %~dp0%script_master%traiterapport_utf8.bat %~dp0%script_poste%traiterapport_iso88591.bat iso-8859-1
		)
	%~dp0%script_poste%traiterapport_iso88591.bat
	goto :eof
)
echo "encodage inconnu. Configurez config.ini"

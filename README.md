Script Windows pour traiter les rapports Aleph au moyen de feuilles de style XSL.
Utile en cas d'impossiblité pour Aleph de traiter le rapport en raison de sa taille.
Paramétrages adaptés aux pratiques d'UCA.

Trois types de rapports pris en compte :
* Rapports Colodus hebdomadaires
* Rapports Pilon mensuels
* Rapports Chargement Sudoc